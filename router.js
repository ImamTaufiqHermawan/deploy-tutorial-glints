const express = require('express');
const router = express.Router();
const User = require('./controller/userController');

router.get('/user', User.show);

module.exports = router;